package app.dao.classes;

import java.util.Date;

/**
 * epam2020_pet_testing
 *
 * @author Alexandr.Lavrinenko@gmail.com
 * @version 1.0
 * @since 13.09.2020
 */
public class User {
    private int user_id;
    private String user_surname;
    private String user_name;
    private String mail;
    private String password;
    private Date createTime;

    public User(int user_id, String user_surname, String user_name, String mail, String password) {
        this.user_id = user_id;
        this.user_surname = user_surname;
        this.user_name = user_name;
        this.mail = mail;
        this.password = password;
    }

    public User(int user_id, String user_surname, String user_name, String mail, String password, Date createTime) {
        this.user_id = user_id;
        this.user_surname = user_surname;
        this.user_name = user_name;
        this.mail = mail;
        this.password = password;
        this.createTime = createTime;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_surname() {
        return user_surname;
    }

    public void setUser_surname(String user_surname) {
        this.user_surname = user_surname;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    // TODO добавить хеширование пароля
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("user_id =").append(user_id);
        sb.append(", user_surname ='").append(user_surname).append('\'');
        sb.append(", user_name ='").append(user_name).append('\'');
        sb.append(", mail ='").append(mail).append('\'');
        sb.append(", password ='").append(password).append('\'');
        sb.append(", createTime =").append(createTime);
        sb.append('}');
        return sb.toString();
    }
}
