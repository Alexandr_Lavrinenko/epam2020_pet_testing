package app.dao.interfaces;

import app.entity.Connector;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

/**
 * Interface with CRUD-methods
 *
 * @author Alexandr.Lavrinenko@gmail.com
 * @version 1.0
 * @since 11.12.2019
 */
public interface Crudable<T> {

    /**
     * The method sets connection according to file settings db.properties.
     *
     * @return connection.
     */
    static Connection getConnection() throws SQLException, IOException {
        Properties props = new Properties();
        try (InputStream in = Connector.class.getClassLoader().getResourceAsStream("db/liquibase.properties")) {
            props.load(in);
        }
        String propDriver = props.getProperty("driver");
        if (propDriver != null) {
            System.setProperty("driver", propDriver);
        } else {
            System.setProperty("driver", "org.mariadb.jdbc.Driver");
        }
        String url = props.getProperty("url");
        String username = props.getProperty("username");
        String password = props.getProperty("password");

        return DriverManager.getConnection(url, username, password);
    }

    /**
     * Method for inserting new rows into a table
     */
    String createRows(List<T> list) throws SQLException, IOException;

    /**
     * Method for reading rows from a table by Id
     */
    List<T> readRowsById(List<Long> list) throws IOException, SQLException;

    /**
     * Method for reading a specified number of rows from a table
     */
    List<T> readRows(int cntRows) throws IOException, SQLException;

    /**
     * Method for changing rows in a table
     */
    String updateRowsById(List<T> clients) throws IOException, SQLException;

    /**
     * Method for deleting rows from a table at the selected Id.
     *
     * @param query - string with the sql query template
     * @param list  - Id list to remove from the table
     * @return returns the result of the operation
     */
    default String deleteRowsById(String query, List<Long> list) throws SQLException, IOException {
        int[] results = null;
        String sourceTable = query.substring(query.indexOf("FROM") + 5, query.indexOf("WHERE") - 1);
        try (Connection conn = Crudable.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(query)) {
            for (Long element : list) {
                // Add each parameter to the row.
                preparedStatement.setLong(1, element);
                // Add row to the batch.
                preparedStatement.addBatch();
            }
            results = preparedStatement.executeBatch();

        }
        return parseResultBatchExec("Deletion rows from " + sourceTable, results);
    }

    /**
     * The method parses the string passed to the input list.
     *
     * @param list - list of Id lines.
     * @return returns a string from Id separated by commas.
     */
    default String parseListToString(List<Long> list) {
        StringBuilder builder = new StringBuilder();
        for (Long id : list) {
            builder.append(id);
            builder.append(",");
        }
        int lastIndex = builder.lastIndexOf(",");
        builder.delete(lastIndex, ++lastIndex);
        return builder.toString();
    }

    /**
     * Method parses SQL batch results into a string
     *
     * @param operation - name of the performed SQL operation
     * @param results   - integer array with mining resultsbatch
     * @return line indicating that the requests have successfully completed or list the numbers of outstanding lines
     */
    default String parseResultBatchExec(String operation, int[] results) {
        StringBuilder resultBuilder = new StringBuilder();
        StringBuilder errorBuilder = new StringBuilder();
        resultBuilder.append(operation);
        resultBuilder.append(": ");
        int errorCounter = 0;
        for (int i = 0; i < results.length; i++) {
            if (results[i] == 0) {
                errorBuilder.append(i + 1);
                errorBuilder.append(", ");
                errorCounter++;
            }
        }
        if (errorCounter == 0) {
            resultBuilder.append("operation completed successfully.");
        } else {
            int lastIndex = errorBuilder.lastIndexOf(",");
            resultBuilder.append("operation failed on: ");
            errorBuilder.delete(lastIndex, ++lastIndex);
            resultBuilder.append(errorBuilder);
            resultBuilder.append("row(s).");
        }
        return resultBuilder.toString();
    }

}
