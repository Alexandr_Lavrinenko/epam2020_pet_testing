package app.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * epam2020_pet_testing
 *
 * @author Alexandr.Lavrinenko@gmail.com
 * @version 1.0
 * @since 01.10.2020
 */
public class DBManager {

    private static final Logger LOG = LoggerFactory.getLogger(DBManager.class);

    // Singleton
    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    /**
     * Returns a DB connection from the Pool Connections. Before using this
     * method you must configure the Date Source and the Connections Pool in your
     * WEB_APP_ROOT/META-INF/context.xml file.
     *
     * @return A DB connection.
     */

	/*
	  Возвращает соединение с БД из соединений пула.
	  Перед использованием этого метода необходимо настроить источник даты и пул подключений
	  в файле WEB_APP_ROOT / META-INF / context.xml.
	  @return Соединение с БД.
	 */
    public Connection getConnection() throws SQLException {
        Connection con = null;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");

            // ST4DB - the name of data source
            DataSource ds = (DataSource) envContext.lookup("jdbc/ST4DB");
            con = ds.getConnection();
        } catch (NamingException ex) {
            LOG.error("Cannot obtain a connection from the pool", ex);
        }
        return con;
    }

    private DBManager() {
    }


    // //////////////////////////////////////////////////////////
    // DB util methods
    // Методы использования БД
    // //////////////////////////////////////////////////////////

    /**
     * Commits and close the given connection.
     *
     * @param con Connection to be committed and closed.
     */

	/*
	   Фиксирует и закрывает данное соединение.
	   @param con
	   Соединение должно быть зафиксировано и закрыто.
	 */
    public void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Rollbacks and close the given connection.
     *
     * @param con Connection to be rollbacked and closed.
     */

	/*
	   Откатывает и закрывает данное соединение.
	   @param con
	   Соединение необходимо откатить и закрыть.
	 */
    public void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
