package app.entity;

import app.dao.interfaces.Crudable;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * epam2020_pet_testing
 *
 * @author Alexandr.Lavrinenko@gmail.com
 * @version 1.0
 * @since 13.09.2020
 */
public class DbWork {
    private Connection connection;

    public DbWork() {
        try {
            connection = Crudable.getConnection();
            System.out.println("OK!");
        } catch (SQLException throwables) {
            System.out.println("Не могу получить Connection");
        } catch (IOException e) {
            System.out.println("Не получается вычитать свойства из файла");
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
