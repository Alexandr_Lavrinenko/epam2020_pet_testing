package app.entity;

import app.dao.classes.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * epam2020_pet_testing
 *
 * @author Alexandr.Lavrinenko@gmail.com
 * @version 1.0
 * @since 13.09.2020
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        DbWork dbWork = new DbWork();
        try (Connection connect = dbWork.getConnection()) {
            stringStatement(connect);
//            stringPrepareStatement(connect);
            stringPrepareStatementGetAll(connect);
            stringPrepareStatementDelete(connect);
        }
    }

    private static void stringPrepareStatementDelete(Connection connect) throws SQLException {
        String query = "delete from user where user_id = ?";
        try (PreparedStatement preparedStatement = connect.prepareStatement(query)) {
            preparedStatement.setInt(1, 11);
            int i = preparedStatement.executeUpdate();
            System.out.println();
            System.out.println(i);
        }
    }

    private static void stringPrepareStatementGetAll(Connection connect) throws SQLException {
        String query = "select * from user where user_id = 15";
        try (PreparedStatement preparedStatement = connect.prepareStatement(query);
             ResultSet rs = preparedStatement.executeQuery();) {

            while (rs.next()) {
                int user_id = rs.getInt("user_id");
                String user_surname = rs.getString("user_surname");
                String user_name = rs.getString("user_name");
                String email = rs.getString("email");
                String password = rs.getString("password");
                Date create_time = rs.getDate("create_time");

                User user = new User(user_id, user_surname, user_name, email, password, create_time);
                System.out.println();
                System.out.println(user.toString());
            }
        }
    }

    private static void stringPrepareStatement(Connection connection) throws SQLException {
        String query = "insert into user (user_id, user_surname, user_name, email, password) values (?,?,?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            int idx = 1;
            preparedStatement.setInt(idx++, 15);
            preparedStatement.setString(idx++, "Pirson");
            preparedStatement.setString(idx++, "Mikky");
            preparedStatement.setString(idx++, "mikky.pirson@nbu.ua");
            preparedStatement.setString(idx, "bushbush");

            boolean execute = preparedStatement.execute();
            System.out.println();
            System.out.println(execute);
        }

    }

    private static void stringStatement(Connection connect) throws SQLException {
        String query = "select * from user";

        List<User> userList;
        try (Statement statement = connect.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            userList = new ArrayList<>();
            while (resultSet.next()) {
                int idx = 1;
                int user_id = resultSet.getInt(idx++);
                String user_surname = resultSet.getString(idx++);
                String user_name = resultSet.getString(idx++);
                String email = resultSet.getString(idx++);
                String password = resultSet.getString(idx++);
                Date create_time = resultSet.getDate(idx++);

                User user = new User(user_id, user_surname, user_name, email, password, create_time);
                userList.add(user);
            }
        }

        for (User usr :userList) {
            System.out.println(usr.toString());
        }
    }

}
