package app.entity;

import app.dao.interfaces.Crudable;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * epam2020_pet_testing
 *
 * @author Alexandr.Lavrinenko@gmail.com
 * @version 1.0
 * @since 13.09.2020
 */
public class Connector implements Crudable {
    public static void main(String[] args) throws IOException, SQLException {
        Connection connection = getConnection();
        Statement statement = getStatement(connection);
        int countExec = statement.executeUpdate("INSERT INTO user (user_id, user_surname, user_name, email, password) " +
                "VALUES (13, 'Dobrinin', 'V''atcheslav', 'slava.dobrinin@dnu.ua', '777')");
        System.out.println("It's OK!" + countExec);
    }

    public static Connection getConnection() throws IOException, SQLException {
        return Crudable.getConnection();
    }

    public static void closeConnection(Connection connection) throws SQLException {
        connection.close();
    }

    public static Statement getStatement(Connection connection) throws SQLException {
        return connection.createStatement();
    }

    /**
     * Method for inserting new rows into a table
     *
     * @param list
     */
    @Override
    public String createRows(List list) throws SQLException, IOException {
        return null;
    }

    /**
     * Method for reading a specified number of rows from a table
     *
     * @param cntRows
     */
    @Override
    public List readRows(int cntRows) throws IOException, SQLException {
        return null;
    }

    /**
     * Method for changing rows in a table
     *
     * @param clients
     */
    @Override
    public String updateRowsById(List clients) throws IOException, SQLException {
        return null;
    }

    /**
     * Method for reading rows from a table by Id
     *
     * @param list
     */
    @Override
    public List readRowsById(List list) throws IOException, SQLException {
        return null;
    }
}
