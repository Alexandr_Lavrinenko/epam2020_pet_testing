-- liquibase formatted sql
-- changeset create_table_sql:0


-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema testing2020
-- -----------------------------------------------------
-- Testing students in several subjects

-- -----------------------------------------------------
-- Schema testing2020
--
-- Testing students in several subjects
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `testing2020` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `testing2020` ;

-- -----------------------------------------------------
-- Table `testing2020`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing2020`.`user` ;

CREATE TABLE IF NOT EXISTS `testing2020`.`user` (
                                                    `user_id` INT UNSIGNED NOT NULL COMMENT 'ID пользователя',
                                                    `user_surname` VARCHAR(45) NOT NULL COMMENT 'Фамилия пользователя',
                                                    `user_name` VARCHAR(16) NOT NULL COMMENT 'Имя пользователя',
                                                    `email` VARCHAR(255) NULL COMMENT 'Почта пользователя',
                                                    `password` LONGTEXT NOT NULL COMMENT 'Секьюрный пароль',
                                                    `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
                                                    PRIMARY KEY (`user_id`))
    ENGINE = InnoDB;

CREATE UNIQUE INDEX `email_UNIQUE` ON `testing2020`.`user` (`email` ASC) VISIBLE;
CREATE UNIQUE INDEX `user_id_UNIQUE` ON `testing2020`.`user` (`user_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `testing2020`.`user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing2020`.`user_role` ;

CREATE TABLE IF NOT EXISTS `testing2020`.`user_role` (
                                                         `user_id` INT UNSIGNED NOT NULL COMMENT 'ID пользователя',
                                                         `user_role` ENUM('admin', 'user') NOT NULL DEFAULT 'user' COMMENT 'Пользовательская роль: администратор или обычный пользователь',
                                                         `user_stasus` ENUM('active', 'delete', 'blocked') NOT NULL DEFAULT 'active' COMMENT 'Статус: активен, удален, блокирован',
                                                         PRIMARY KEY (`user_id`),
                                                         CONSTRAINT `fk_user_role_user`
                                                             FOREIGN KEY (`user_id`)
                                                                 REFERENCES `testing2020`.`user` (`user_id`)
                                                                 ON DELETE CASCADE
                                                                 ON UPDATE CASCADE)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `testing2020`.`discipline`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing2020`.`discipline` ;

CREATE TABLE IF NOT EXISTS `testing2020`.`discipline` (
                                                          `discipline_id` VARCHAR(5) NOT NULL COMMENT 'Идентификатор предмета',
                                                          `discipline_name` VARCHAR(100) NULL COMMENT 'Название предмета',
                                                          `discipline_test_cnt` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Количество тестов по данному предмету',
                                                          PRIMARY KEY (`discipline_id`))
    ENGINE = InnoDB;

CREATE UNIQUE INDEX `discipline_id_UNIQUE` ON `testing2020`.`discipline` (`discipline_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `testing2020`.`test_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing2020`.`test_list` ;

CREATE TABLE IF NOT EXISTS `testing2020`.`test_list` (
                                                         `test_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID теста',
                                                         `test_name` VARCHAR(100) NOT NULL COMMENT 'Наименование теста',
                                                         `test_quest_cnt` INT UNSIGNED NOT NULL COMMENT 'Количество вопросов',
                                                         `test_passing_score` DECIMAL(5,2) NULL COMMENT 'Проходной балл',
                                                         `discipline_id` VARCHAR(5) NOT NULL,
                                                         `test_difficulty` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Сложность теста 0 - очень легкий, 10 - очень сложный',
                                                         PRIMARY KEY (`test_id`, `discipline_id`),
                                                         CONSTRAINT `fk_test_list_discipline1`
                                                             FOREIGN KEY (`discipline_id`)
                                                                 REFERENCES `testing2020`.`discipline` (`discipline_id`)
                                                                 ON DELETE CASCADE
                                                                 ON UPDATE CASCADE)
    ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_test_UNIQUE` ON `testing2020`.`test_list` (`test_id` ASC) VISIBLE;
CREATE UNIQUE INDEX `test_name_UNIQUE` ON `testing2020`.`test_list` (`test_name` ASC) VISIBLE;
CREATE INDEX `fk_test_list_discipline1_idx` ON `testing2020`.`test_list` (`discipline_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `testing2020`.`test_question`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing2020`.`test_question` ;

CREATE TABLE IF NOT EXISTS `testing2020`.`test_question` (
                                                             `test_id` INT UNSIGNED NOT NULL COMMENT 'ID теста',
                                                             `test_question_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID вопроса',
                                                             `test_question_status` ENUM('active', 'deleted') NOT NULL DEFAULT 'active' COMMENT 'Статус вопроса - активен или удален',
                                                             `test_question` TEXT(1000) NOT NULL COMMENT 'Формулировка вопроса',
                                                             PRIMARY KEY (`test_question_id`),
                                                             CONSTRAINT `fk_test_question_test_list1`
                                                                 FOREIGN KEY (`test_id`)
                                                                     REFERENCES `testing2020`.`test_list` (`test_id`)
                                                                     ON DELETE CASCADE
                                                                     ON UPDATE CASCADE)
    ENGINE = InnoDB;

CREATE UNIQUE INDEX `test_question_id_UNIQUE` ON `testing2020`.`test_question` (`test_question_id` ASC) VISIBLE;
CREATE INDEX `fk_test_question_test_list1_idx` ON `testing2020`.`test_question` (`test_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `testing2020`.`test_answer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing2020`.`test_answer` ;

CREATE TABLE IF NOT EXISTS `testing2020`.`test_answer` (
                                                           `test_answer` TEXT(2000) NULL COMMENT 'текст ответа на вопрос',
                                                           `test_answer_id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'идентификатор ответа конкретного теста (связь через test_question_id)',
                                                           `test_question_id` INT UNSIGNED NOT NULL COMMENT 'ID вопроса теста',
                                                           `test_answer_score` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 - неверный ответ, 1 - верный ответ',
                                                           `test_answer_status` ENUM('active', 'deleted') NULL DEFAULT 'active' COMMENT 'активный ответ отображается в доступных ответах, неактивный - нет',
                                                           PRIMARY KEY (`test_answer_id`),
                                                           CONSTRAINT `fk_test_answer_test_question1`
                                                               FOREIGN KEY (`test_question_id`)
                                                                   REFERENCES `testing2020`.`test_question` (`test_question_id`)
                                                                   ON DELETE NO ACTION
                                                                   ON UPDATE NO ACTION)
    ENGINE = InnoDB;

CREATE INDEX `fk_test_answer_test_question1_idx` ON `testing2020`.`test_answer` (`test_question_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `testing2020`.`user_test_passed`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing2020`.`user_test_passed` ;

CREATE TABLE IF NOT EXISTS `testing2020`.`user_test_passed` (
                                                                `user_id` INT UNSIGNED NOT NULL COMMENT 'ID пользователя из т. user',
                                                                `test_id` INT UNSIGNED NOT NULL COMMENT 'ID теста из т. test_list',
                                                                `test_passed_date` DATE NOT NULL COMMENT 'Дата успешного прохождения',
                                                                `user_test_passed_scor` DECIMAL(3,2) UNSIGNED NOT NULL COMMENT 'Набранный проходной балл',
                                                                `discipline_id` VARCHAR(5) NOT NULL COMMENT 'ID предмета',
                                                                PRIMARY KEY (`test_id`, `discipline_id`),
                                                                CONSTRAINT `fk_user_test_passed_user1`
                                                                    FOREIGN KEY (`user_id`)
                                                                        REFERENCES `testing2020`.`user` (`user_id`)
                                                                        ON DELETE NO ACTION
                                                                        ON UPDATE NO ACTION,
                                                                CONSTRAINT `fk_user_test_passed_test_list1`
                                                                    FOREIGN KEY (`test_id` , `discipline_id`)
                                                                        REFERENCES `testing2020`.`test_list` (`test_id` , `discipline_id`)
                                                                        ON DELETE NO ACTION
                                                                        ON UPDATE NO ACTION)
    ENGINE = InnoDB;

CREATE INDEX `fk_user_test_passed_user1_idx` ON `testing2020`.`user_test_passed` (`user_id` ASC) VISIBLE;
CREATE INDEX `fk_user_test_passed_test_list1_idx` ON `testing2020`.`user_test_passed` (`test_id` ASC, `discipline_id` ASC) VISIBLE;
CREATE UNIQUE INDEX `test_id_UNIQUE` ON `testing2020`.`user_test_passed` (`test_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- insert in user
INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (1, 'KulchitsвЂ™kiy', 'Ivan', 'kulchitsвЂ™kiy.ivan@dnu.ua', '12345', '2020-09-01 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (2, 'GrirorвЂ™ev', 'Petr', 'grirorвЂ™ev.petr@dnu.ua', '12345', '2020-09-02 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (3, 'Mihaylov', 'Alexey', 'mihaylov.alexey@dnu.ua', '12345', '2020-09-03 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (4, 'Orlov', 'Grigoriy', 'orlov.grigoriy@dnu.ua', '12345', '2020-09-04 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (5, 'Haskina', 'Inna', 'haskina.inna@dnu.ua', '12345', '2020-09-05 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (6, 'Koval', 'Olga', 'koval.olga@dnu.ua', '12345', '2020-09-06 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (7, 'Stets', 'Veronika', 'stets.veronika@dnu.ua', '12345', '2020-09-07 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (8, 'Ivolgina', 'Nanalja', 'ivolgina.nanalja@dnu.ua', '12345', '2020-09-08 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (9, 'Beroev', 'Igor', 'beroev.igor@dnu.ua', '12345', '2020-09-09 00:00:00');

INSERT INTO testing2020.user (user_id, user_surname, user_name, email, password, create_time)
VALUES (10, 'Belih', 'Eror', 'belih.eror@dnu.ua', '12345', '2020-09-10 00:00:00');

-- insert in user_role
INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (1, 'admin', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (2, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (3, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (4, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (5, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (6, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (7, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (8, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (9, 'user', 'active');

INSERT INTO testing2020.user_role (user_id, user_role, user_stasus)
VALUES (10, 'user', 'active');

-- insert in discipline
INSERT INTO testing2020.discipline (discipline_id, discipline_name, discipline_test_cnt)
VALUES ('BLGY1', 'Biology', 1);

INSERT INTO testing2020.discipline (discipline_id, discipline_name, discipline_test_cnt)
VALUES ('PRG01', 'Programming', 1);

INSERT INTO testing2020.discipline (discipline_id, discipline_name, discipline_test_cnt)
VALUES ('PRG02', 'Informatics', 1);

-- insert in test_list
INSERT INTO testing2020.test_list (test_id, test_name, test_quest_cnt, test_passing_score, discipline_id,test_difficulty)
VALUES (1, 'Biology Basic', 10, 75.00, 'BLGY1', 0);

INSERT INTO testing2020.test_list (test_id, test_name, test_quest_cnt, test_passing_score, discipline_id,test_difficulty)
VALUES (2, 'Java Core', 10, 80.00, 'PRG01', 5);

INSERT INTO testing2020.test_list (test_id, test_name, test_quest_cnt, test_passing_score, discipline_id,test_difficulty)
VALUES (3, 'Informatics Basic', 12, 60.00, 'PRG02', 2);

-- insert in test_question
INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 1, 'active', 'What is inanimate matter?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 2, 'active', 'What substances are needed for photosynthesis?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 3, 'active', 'What organisms are autotrophs?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 4, 'active', 'What is metabolism?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 5, 'active', 'What is part of metabolism?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 6, 'active', 'What happens during metabolism?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 7, 'active', 'What organisms are a transitional form from inanimate to living matter?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 8, 'active', 'How do bacteria differ from all other organisms?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 9, 'active', 'Which organism belongs to the animal kingdom?');

INSERT INTO testing2020.test_question (test_id, test_question_id, test_question_status, test_question)
VALUES (1, 10, 'active', 'What elements are needed to sustain life?');

-- insert in test_answer
INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Bacteria', 1, 1, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Mushrooms', 2, 1, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Plants', 3, 1, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Mountains', 4, 1, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Water, carbon dioxide', 5, 2, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Sunlight, minerals', 6, 2, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Water, sunlight, carbon dioxide', 7, 2, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Other living things', 8, 2, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Mushrooms', 9, 3, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Bacteria', 10, 3, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Plants', 11, 3, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Animals', 12, 3, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Reproductive ability', 13, 4, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Metabolism', 14, 4, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Growth process', 15, 4, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Nutrition', 16, 4, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Height', 17, 5, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Irritability', 18, 5, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Breathing', 19, 5, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Reproduction', 20, 5, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Simple substances coming from the external environment are transformed into complex', 21, 6, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Simple substances are formed from complex substances in the cell', 22, 6, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Complex substances break down into simple ones, from which the cell forms new substances', 23, 6, 10,'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Complex substances are used by the cell unchanged', 24, 6, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Bacteria', 25, 7, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Mushrooms', 26, 7, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('The simplest', 27, 7, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Viruses', 28, 7, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('The presence of a core', 29, 8, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Lack of core', 30, 8, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('The ability to divide', 31, 8, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Ability to move', 32, 8, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Amoeba', 33, 9, 10, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Fly agaric', 34, 9, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Chamomile', 35, 9, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('All of the above', 36, 9, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Oxygen', 37, 10, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Nitrogen', 38, 10, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('Carbon', 39, 10, 0, 'active');

INSERT INTO testing2020.test_answer (test_answer, test_answer_id, test_question_id, test_answer_score,test_answer_status)
VALUES ('All of the above', 40, 10, 10, 'active');

